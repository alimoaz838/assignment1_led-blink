#include <Arduino.h>

// In this assignment, we have learnt that how to use DDRxn, PORTxn resgiter and turn on and off any output 
// by using system delay function

void setup() 
{
  DDRB = 1<<5;    // Setting the direction of digital pin 13 or bit 5 of portB - OUTPUT
}
void loop() 
{
  PORTB = 1<<5;   // Writing one to digital pin 13 or bit 5 of portB
  delay(1000);    // delay of one second  or 1000 msec
  PORTB = 0<<5;   // Writing zero to digital pin 13 or bit 5 of portB
  delay(1000);    // delay of one second  or 1000 msec
}